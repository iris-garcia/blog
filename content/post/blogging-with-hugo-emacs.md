+++
title = "Blogging from emacs: Hugo & Org"
author = ["Iris Garcia"]
lastmod = 2019-04-07T16:59:52+02:00
tags = ["blogging", "emacs"]
categories = ["Emacs"]
draft = false
asciinema = true
+++

This is my first post, and instead of writting just a _hello world_ post I
wanted to talk about my blogging workflow which I find much more
interesting than a simple hello world.

<!--more-->

As the tittle says I use emacs for blogging and actually for everything I
can (which is a lot :laughing:), but that will come in future posts.

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [How to set it up (Spacemacs)](#how-to-set-it-up--spacemacs)
- [Exporting images](#exporting-images)
- [Using Footnotes](#using-footnotes)
- [Exporting literal HTML](#exporting-literal-html)
- [Using shortcodes](#using-shortcodes)
- [Inserting videos](#inserting-videos)
- [Inserting Asciinema casts](#inserting-asciinema-casts)

</div>
<!--endtoc-->


## How to set it up (Spacemacs) {#how-to-set-it-up--spacemacs}

1.  Install the hugo binary ([Hugo install guide](https://gohugo.io/getting-started/installing/)), using Arch linux:

    ```shell
    $ sudo pacman -S hugo
    ```

2.  Start a new site and create a folder to host our org files:

    ```shell
    $ hugo new site blog
    $ mkdir blog/content-org
    ```

3.  Enable org hugo support when loading the org layer:

    ```emacs-lisp
    dotspacemacs-configuration-layers
    '(
      (org :variables
           org-enable-hugo-support t)
      )
    ```

4.  Download a theme inside the **themes** folder:

    ```bash
    $ git submodule add https://github.com/vaga/hugo-theme-m10c.git themes/m10c
    ```

5.  Configure hugo's site (`config.toml`):

    ```yaml
    baseURL = "https://iris-garcia.github.io/"
    languageCode = "en-us"
    title = "Iris Garcia"
    theme = "beautifulhugo"
    enableEmoji = true
    enableGitInfo = true
    paginate = 20
    publishDir = "public"
    pygmentsCodeFences = true
    pygmentsStyle = "native"
    footnoteReturnLinkContents = "↩"

    [permalinks]
      post = "/posts/:year-:month-:day-:title"

    [author]
      name = "Iris Garcia"
      email = "iris.garcia.desebastian@gmail.com"
      linkedin = "https://www.linkedin.com/in/iris-g/"
      github = "https://github.com/iris-garcia"

    [params]
      subtitle = "DevOps stuff"
    #  logo = ""
    #  favicon = ""
      description = "Senior DevOps Engineer"
      [[params.social]]
        name = "github"
        url = ""
      [[params.social]]
        name = "linkedin"
        url = ""
      [[params.social]]
        name = "mail"
        url = "mailto:iris.garcia.desebastian@gmail.com"

    [[menu.main]]
      name = "Posts"
      url = "post"
      weight = 1

    [[menu.main]]
      name = "Tags"
      url = "tags"
      weight = 2
    ```


## Exporting images {#exporting-images}

it should be centered

{{< figure src="/ox-hugo/acurable.png" alt="cat/spider image" caption="Figure 1: An image" title="action!" width="100%" >}}


## Using Footnotes {#using-footnotes}

Referencing the footnote[^fn:1]


## Exporting literal HTML {#exporting-literal-html}


### Inline syntax {#inline-syntax}

```org
@@html:<b>@@This is bold text@@html:</b>@@
```

<b>This is bold text</b>


### For larger code blocks {#for-larger-code-blocks}

```org
#+HTML: <b class="example">Literal HTML code for export</b>
```

<b class="example">Literal HTML code for export</b>

```org
#+BEGIN_EXPORT html
<ul>
<li>One</li>
<li>Two</li>
<li>Three</li>
</ul>
#+END_EXPORT

```

<ul>
<li>One</li>
<li>Two</li>
<li>Three</li>
</ul>


## Using shortcodes {#using-shortcodes}

{{< highlight go >}} System.Println("Oh yeah") {{< /highlight >}}


## Inserting videos {#inserting-videos}

-   Using Hugo's shortcode
    {{< youtube gfZDwYeBlO4 >}}

-   From a static file using html code

    <video width="100%" controls>
      <source src="/ox-hugo/small.mp4" type="video/mp4">
      Your browser does not support HTML5 video.
    </video>


## Inserting Asciinema casts {#inserting-asciinema-casts}

Sometimes it is nice to record and show a set of shell commands instead of
writting all of them in plain text, to achieve this there is a great tool
called [asciinema](https://asciinema.org).

With the following configuration it is possible to include asciinema's
casts in any post.

1.  Add the asciinema's `css` and `js` files, if the theme used allows
    overriding of `<head></head>` and `<body></body>` like the one I
    currently use [beautifulhugo](https://themes.gohugo.io/beautifulhugo/), which has the following layouts partials:

    <a id="code-snippet--partials-files"></a>
    {{< highlight raw "hl_lines=4 6" >}}
    $ ls -l themes/beautifulhugo/layouts/partials                                                                            01:26    96% 
    total 60
    -rw-r--r-- 1 user user  266 Apr  6 11:30 disqus.html
    -rw-r--r-- 1 user user  255 Apr  6 11:30 footer_custom.html
    -rw-r--r-- 1 user user 6574 Apr  6 11:30 footer.html
    -rw-r--r-- 1 user user  251 Apr  6 11:30 head_custom.html
    -rw-r--r-- 1 user user 3384 Apr  6 11:30 header.html
    -rw-r--r-- 1 user user 5796 Apr  6 11:30 head.html
    -rw-r--r-- 1 user user 2227 Apr  6 11:30 load-photoswipe-theme.html
    -rw-r--r-- 1 user user 3536 Apr  6 11:30 nav.html
    -rw-r--r-- 1 user user 1828 Apr  6 16:38 post_meta.html
    drwxr-xr-x 3 user user 4096 Apr  6 11:30 seo
    -rw-r--r-- 1 user user 1896 Apr  6 11:30 share-links.html
    -rw-r--r-- 1 user user 3077 Apr  6 11:30 staticman-comments.html
    -rw-r--r-- 1 user user   99 Apr  6 11:30 translation_link.html
    {{< /highlight >}}


    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--partials-files">Code Snippet 1</a></span>:
      partials files
    </div>

    <a id="code-snippet--head-custom.html"></a>
    ```html
    {{ if .Params.asciinema }}
    <link rel="stylesheet" type="text/css" href="{{ .Site.BaseURL }}css/asciinema-player.css" />
    {{ end }}

    <link rel="stylesheet" type="text/css" href="{{ .Site.BaseURL }}css/custom.css" />
    ```

    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--head-custom.html">Code Snippet 2</a></span>:
      head_custom.html
    </div>

    <a id="code-snippet--footer-custom.html"></a>
    ```html
    {{ if .Params.asciinema }}
    <script src="{{ .Site.BaseURL }}js/asciinema-player.js"></script>
    {{ end }}
    ```

    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--footer-custom.html">Code Snippet 3</a></span>:
      footer_custom.html
    </div>

2.  Add a [shortcode](https://gohugo.io/content-management/shortcodes/) under `layouts/shortcodes` folder.

    <a id="code-snippet--asciinema shortcode"></a>
    ```go
    <p>
        <asciinema-player
            src="/casts/{{ with .Get "key" }}{{ . }}{{ end }}.cast"
            cols="{{ if .Get "cols" }}{{ .Get "cols" }}{{ else }}640{{ end }}"
            rows="{{ if .Get "rows" }}{{ .Get "rows" }}{{ else }}10{{ end }}"
            {{ if .Get "autoplay" }}autoplay="{{ .Get "autoplay" }}"{{ end }}
            {{ if .Get "preload" }}preload="{{ .Get "preload" }}"{{ end }}
            {{ if .Get "loop" }}loop="{{ .Get "loop" }}"{{ end }}
            start-at="{{ if .Get "start-at" }}{{ .Get "start-at" }}{{ else }}0{{ end }}"
            speed="{{ if .Get "speed" }}{{ .Get "speed" }}{{ else }}1{{ end }}"
            {{ if .Get "idle-time-limit" }}idle-time-limit="{{ .Get "idle-time-limit" }}"{{ end }}
            {{ if .Get "poster" }}poster="{{ .Get "poster" }}"{{ end }}
            {{ if .Get "font-size" }}font-size="{{ .Get "font-size" }}"{{ end }}
            {{ if .Get "theme" }}theme="{{ .Get "theme" }}"{{ end }}
            {{ if .Get "title" }}title="{{ .Get "title" }}"{{ end }}
            {{ if .Get "author" }}author="{{ .Get "author" }}"{{ end }}
            {{ if .Get "author-url" }}author-url="{{ .Get "author-url" }}"{{ end }}
            {{ if .Get "author-img-url" }}author-img-url="{{ .Get "author-img-url" }}"{{ end }}
        ></asciinema-player>
    </p>
    ```

    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--asciinema shortcode">Code Snippet 4</a></span>:
      Asciinema shortcode
    </div>

3.  Record the cast under `static/casts` folder.

    ```shell
    $ asciinema rec static/casts/test.cast
    ```

4.  Then just insert the shortcode:

    ```nil
    {{</* asciinema key="test" rows="25" preload="1" */>}}
    ```

    {{< asciinema key="test" rows="25" preload="1" >}}

[^fn:1]: This is a footnote.
